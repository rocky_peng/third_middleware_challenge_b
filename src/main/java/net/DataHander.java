package net;

import io.netty.buffer.ByteBuf;

/**
 * Author :  Rocky
 * Date : 16/06/2017 14:15
 * Description :
 * Test :
 */
public interface DataHander {

    void handleChangeRecord(ByteBuf changeRecordBuf);

    void handleRecordMeta(ByteBuf recordMeta);

    void getServerResultComplete();

    void handlePkStartAndEnd(ByteBuf buf);

    void handleServerResultData(ByteBuf serverResultDataBuf);
}
