package net;

import dev.RecordMeta;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

/**
 * Author :  Rocky
 * Date : 15/06/2017 16:03
 * Description :
 * Test :
 */
@Slf4j
public class ClientTest {

    public static void main(String[] args) {
        NetworkClient client = new NetworkClient("127.0.0.1", 33333, new DataHander() {

            @Override
            public void handleChangeRecord(ByteBuf changeRecordBuf) {

                byte opt = changeRecordBuf.readByte();

                switch (opt) {
                    case 0:
                        System.out.println("insert");

                        long pk = changeRecordBuf.readLong();
                        long col1 = changeRecordBuf.readLong();

                        byte[] a = new byte[changeRecordBuf.readShort()];
                        changeRecordBuf.readBytes(a);
                        String col2 = new String(a);

                        System.out.println(pk == 1000000);
                        System.out.println(col1 == 1);
                        System.out.println(col2.equals("pqs"));
                        break;
                    case 1:
                        System.out.println("update");

                        long oldPk = changeRecordBuf.readLong();

                        long newPk = changeRecordBuf.readLong();

                        System.out.println(oldPk + "  " + newPk);

                        byte updateColumnCount = changeRecordBuf.readByte();

                        for (byte i = 0; i < updateColumnCount; i++) {
                            byte updatedColumnNameByte = changeRecordBuf.readByte();

                            String updatedColumnName = RecordMeta.decodeFieldName(updatedColumnNameByte);

                            long oldValue = changeRecordBuf.readLong();

                            long newValue = changeRecordBuf.readLong();

                            System.out.println(updatedColumnName + "  " + oldValue + "  " + newValue);
                        }
                        break;
                    case 2:
                        long delPk = changeRecordBuf.readLong();

                        System.out.println(delPk);
                        break;
                    default:
                        System.out.println("wrong opt");
                }
            }

            @Override
            public void handleRecordMeta(ByteBuf recordMeta) {
                //元数据解码
                RecordMeta.decode(recordMeta);
                RecordMeta.encodeFieldNames();
            }

            @Override
            public void getServerResultComplete() {

            }

            @Override
            public void handlePkStartAndEnd(ByteBuf buf) {

            }

            @Override
            public void handleServerResultData(ByteBuf serverResultDataBuf) {

            }
        });
    }
}
