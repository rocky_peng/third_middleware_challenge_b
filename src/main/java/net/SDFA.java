package net;

import dev.MiscUtils;
import io.netty.buffer.ByteBuf;

/**
 * Author :  Rocky
 * Date : 18/06/2017 06:43
 * Description :
 * Test :
 */
public class SDFA {

    /*public static void main(String[] args) {
        long pkOldValue = Long.MAX_VALUE;

        int msgMaxCountPerMsgFile = 53687091;

        int recordBytes = 40;

        //计算这个pk的行记录应该保存在哪个行记录文件中
        int targetMsgMbbIndex = ((int) ((pkOldValue - 1) / msgMaxCountPerMsgFile));

        //计算这条记录在这个行记录文件中的位置

        long count = targetMsgMbbIndex * 1L * msgMaxCountPerMsgFile;

        long pkTemp = pkOldValue - count - 1;

        long post = pkTemp * recordBytes + 4;

        int destPost = ((int) ((pkOldValue - targetMsgMbbIndex * msgMaxCountPerMsgFile - 1) * recordBytes)) + 4;

        System.out.println(destPost);
    }*/


    public static void main(String[] args) {

        System.out.println();

        ByteBuf b = MiscUtils.directBuffer(1111);
        b.writeMedium(-1);
        b.readUnsignedMedium();

        long pkOldValue = 53685729;

        int msgMaxCountPerMsgFile = 53687091;

        int recordBytes = 40;

        //计算这个pk的行记录应该保存在哪个行记录文件中
        int targetMsgMbbIndex = ((int) ((pkOldValue - 1) / msgMaxCountPerMsgFile));

        //计算这条记录在这个行记录文件中的位置
        int destPost = ((int) ((pkOldValue - targetMsgMbbIndex * msgMaxCountPerMsgFile - 1) * recordBytes)) + 4;

        System.out.println(destPost);
    }
}
