package net;

import dev.MapEntry;
import dev.RecordMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author :  Rocky
 * Date : 15/06/2017 16:03
 * Description :
 * Test :
 */
public class ServerTest {


    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            RecordMeta.addFieldDataType(i + "", "" + i);
            RecordMeta.fieldsOffset.put("" + i, i);
        }
        RecordMeta.encodeFieldNames();

        RecordMeta.sizePerRecord = RecordMeta.fieldsOffset.size();
        NetworkServer server = new NetworkServer(33333);

        //发送insert
        List<Object> insertColumns = new ArrayList();
        insertColumns.add(1l);
        insertColumns.add("pqs".getBytes());
        server.sendInsert(1000000, insertColumns);
        server.flush();

        //发送update
        Map<String, MapEntry> updatedColumns = new HashMap<>();
        updatedColumns.put("1", new MapEntry(1l, 100l));
        updatedColumns.put("2", new MapEntry(2l, 200l));
        server.sendUpdate(1000000, 1000001, updatedColumns);
        server.flush();

        //发送delete
        server.sendDelete(1000000);
        server.flush();
        server.sendEndFlag();

        System.out.println("完成");
    }
}
