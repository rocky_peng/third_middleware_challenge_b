package bplus;

public interface B {
    //查询
    Object get(Comparable key);

    //移除
    void remove(Comparable key);

    //插入或者更新，如果已经存在，就更新，否则插入
    void insertOrUpdate(Comparable key, Object obj);
} 