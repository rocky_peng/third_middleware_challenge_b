package bplus;

import java.util.Random;

/**
 * Author :  Rocky
 * Date : 07/06/2017 11:04
 * Description :
 * Test :
 */
public class BPlusTreeTest {

    //测试
    public static void main(String[] args) {
        BPlusTree tree = new BPlusTree(6);
        Random random = new Random();
        long current = System.currentTimeMillis();
        for (int j = 0; j < 100000; j++) {
            for (int i = 0; i < 100; i++) {
                int randomNumber = random.nextInt(1000);
                tree.insertOrUpdate(randomNumber, randomNumber);
            }

            for (int i = 0; i < 100; i++) {
                int randomNumber = random.nextInt(1000);
                tree.remove(randomNumber);
            }
        }

        long duration = System.currentTimeMillis() - current;
        System.out.println("time elpsed for duration: " + duration);
        int search = 80;
        System.out.print(tree.get(search));
    }
}
