package dev;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * Author :  Rocky
 * Date : 14/06/2017 15:21
 * Description :
 * Test :
 */
public class ReadTest {


    public static void main(String[] args) throws IOException {

        /*RandomAccessFile raf = new RandomAccessFile("/Users/zuji/Desktop/sync/pqs.txt", "r");
        MappedByteBuffer msgMbb = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, raf.length());

        ByteBuffer dd = ByteBuffer.allocate(1000000);
        byte sd;
        while ((sd = msgMbb.get()) != 10) {
            dd.put(sd);
        }

        String str = new String(dd.array(), 0, dd.position());
        System.out.println(str);*/


        /*RandomAccessFile raf = new RandomAccessFile("/Users/zuji/Desktop/sync/middle/78706iroxp/0.msg", "r");
        MappedByteBuffer msgMbb = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, 4, raf.readInt() - 4);

        raf = new RandomAccessFile("/Users/zuji/Desktop/sync/middle/78706iroxp/0.str", "r");
        MappedByteBuffer strMbb = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, raf.length());

//        msgMbb.position(1820130977-4);

        ByteBuffer dd = ByteBuffer.allocate(1000 * 1024);
        while (msgMbb.hasRemaining()) {

            //读取标志位
            byte flag = readFlag(msgMbb);
            if (!isValidFlag(flag)) {
                msgMbb.position(msgMbb.position() + 18);
                continue;
            }

            long a = readLong(msgMbb);
            byte[] a1 = (a + "").getBytes();

            byte[] b = readStringBytes(msgMbb, strMbb);

            byte[] c = readStringBytes(msgMbb, strMbb);

            dd.put(a1);
            dd.put((byte) 9);
            dd.put(b);
            dd.put((byte) 9);
            dd.put(c);
            dd.put((byte) 10);
        }

        raf = new RandomAccessFile("/Users/zuji/Desktop/sync/middle/78706iroxp/8.msg", "r");
        msgMbb = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, 4, raf.readInt() - 4);
        while (msgMbb.hasRemaining()) {

            //读取标志位
            byte flag = readFlag(msgMbb);
            if (!isValidFlag(flag)) {
                msgMbb.position(msgMbb.position() + 18);
                continue;
            }

            long a = readLong(msgMbb);
            byte[] a1 = (a + "").getBytes();

            byte[] b = readStringBytes(msgMbb, strMbb);

            byte[] c = readStringBytes(msgMbb, strMbb);

            dd.put(a1);
            dd.put((byte) 9);
            dd.put(b);
            dd.put((byte) 9);
            dd.put(c);
            dd.put((byte) 10);
        }

        RandomAccessFile raf1 = new RandomAccessFile("/Users/zuji/Desktop/sync/pqs1.txt", "rw");
        MappedByteBuffer msgMbb1 = raf1.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, dd.position());

        dd.flip();
        msgMbb1.put(dd);


        raf1.close();*/


        /*

        String pqs1Md5 = getMd5ByFile(new File("/Users/zuji/Desktop/sync/sync_results/cc443ea9-da2c-4c39-acb5-5f85ede5fcfb/Result.rs"));
        String pqsMd5 = getMd5ByFile(new File("/Users/zuji/Desktop/pqs.txt"));

        System.out.println(pqs1Md5.equals(pqsMd5));*/

        long s = System.currentTimeMillis();
        Map<Long, Long> map = new HashMap<>();
        for (long i = 0; i < 10000000; i++) {
            map.put(i, i);

        }

        System.out.println(System.currentTimeMillis() - s);
        s = System.currentTimeMillis();

        for (long i = 0; i < 10000000; i++) {
            map.get(i);
        }

        System.out.println(System.currentTimeMillis() - s);

//        System.out.println(5192708 * 8);

//        System.out.println(Double.valueOf(Math.pow(2, 24) - 1).longValue());


//        MiscUtils.directBuffer(20).writeMedium(22);


    }


    private static boolean isValidFlag(byte flag) {
        byte a = (byte) (flag & Byte.MIN_VALUE);
        return a == Byte.MIN_VALUE;
    }

    private static byte[] readStringBytes(MappedByteBuffer msgMbb, MappedByteBuffer strMbb) {
        //读取字符串所在的文件编号
        byte strFileIndex = msgMbb.get();

        //字符串所在的偏移量
        int strPos = msgMbb.getInt();

        strMbb.position(strPos);

        int strByteSize = MiscUtils.toUnsignedInt(strMbb.getShort());
        byte[] strBytes = new byte[strByteSize];
        strMbb.get(strBytes);

        return strBytes;
    }

    private static long readLong(MappedByteBuffer msgMbb) {
        long pk = msgMbb.getLong();
        return pk;
    }

    private static byte readFlag(MappedByteBuffer msgMbb) {
        return msgMbb.get();
    }
}
