package dev;

import io.netty.buffer.ByteBuf;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * Author :  Rocky
 * Date : 16/06/2017 10:55
 * Description :
 * Test :
 */
public class RecordMetaEncodeTest {


    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            RecordMeta.addFieldDataType(i + "", "" + i);
            RecordMeta.fieldsOffset.put("" + i, i);
        }

        RecordMeta.sizePerRecord = RecordMeta.fieldsOffset.size();


        ByteBuf bb = RecordMeta.encode();

        //
        RecordMeta.sizePerRecord = -1;

        RecordMeta.fieldsDataTypeList.clear();

        RecordMeta.fieldsDataTypeMap.clear();

        RecordMeta.fieldsOffset.clear();

        RecordMeta.decode(bb);

        System.out.println();

    }


    @Test
    public void f() {
        ByteBuf bb = MiscUtils.directBuffer(100);

//        bb.writeCharSequence("HELLO", Charset.forName("utf-8"));

        System.out.println(bb);
    }
}
