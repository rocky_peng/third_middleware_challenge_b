package dev;

import com.alibaba.middleware.race.sync.Client;
import com.alibaba.middleware.race.sync.Constants2;
import net.DataHander;
import net.NetworkClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientV1 {

    private static final Logger log = LoggerFactory.getLogger(Client.class);

    public static void main(String[] args) throws Exception {
        if (args == null || args.length == 0) {
            args = new String[]{"127.0.0.1"};
        }

        String ip = args[0];

        DataHander clientDataHander = new ClientDataHander();
        NetworkClient client = new NetworkClient(ip, Constants2.SERVER_PORT, clientDataHander);
    }


}
