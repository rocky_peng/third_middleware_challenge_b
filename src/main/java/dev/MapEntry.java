package dev;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Map;

/**
 * Author :  Rocky
 * Date : 14/06/2017 22:19
 * Description :
 * Test :
 */
public class MapEntry implements Map.Entry {

    private Object key;

    private Object value;

    public MapEntry(Object key, Object value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public Object getKey() {
        return key;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public Object setValue(Object value) {
        throw new NotImplementedException();
    }
}
