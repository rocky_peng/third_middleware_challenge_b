package com.alibaba.middleware.race.sync;

import dev.ClientV1;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Client {

    public static void main(String[] args) throws Exception {

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("彭青松");
            }
        }));
        ClientV1.main(args);
    }

}
