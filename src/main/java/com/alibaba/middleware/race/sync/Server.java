package com.alibaba.middleware.race.sync;

import dev.ServerV1;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Server {


    public static void main(String[] args) {
        long s = System.currentTimeMillis();
        log.info("pqs");
        try {
            ServerV1.main(args);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        log.info("完成，耗时：" + (System.currentTimeMillis() - s) + " ms");
    }

}
