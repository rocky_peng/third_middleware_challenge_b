package com.alibaba.middleware.race.sync;

import java.util.UUID;

/**
 * 外部赛示例代码需要的常量 Created by wanshao on 2017/5/25.
 */
@Deprecated
interface Constants {

    ////////////////////////////// 本地测试环境  //////////////////////////////
    // server端口
    Integer LOCAL_SERVER_PORT = 5527;

    String LOCAL_HOME = "/Users/zuji/Desktop/sync";
//    String LOCAL_HOME = "/Users/zuji/Desktop/sync2";

    // teamCode
    String LOCAL_TEAM_CODE = UUID.randomUUID().toString();
//    String LOCAL_TEAM_CODE = "78706iroxp";

    //// 赛题数据：1.txt到10.txt
    String LOCAL_DATA_HOME = LOCAL_HOME + "/canal_data/";

    //日志目录
    String LOCAL_LOG_HOME = LOCAL_HOME + "/logs/" + LOCAL_TEAM_CODE + "/";

    // 中间结果目录（client和server都会用到）
    String LOCAL_MIDDLE_HOME = LOCAL_HOME + "/middle/" + LOCAL_TEAM_CODE + "/";

    // 结果文件目录(client端会用到)
    String LOCAL_RESULT_HOME = LOCAL_HOME + "/sync_results/" + LOCAL_TEAM_CODE + "/";

    //结果文件的命名
    String LOCAL_RESULT_FILE_NAME = "Result.rs";


    ////////////////////////////// 线上比赛环境  //////////////////////////////
    // server端口
    Integer PROD_SERVER_PORT = 5527;

    String PROD_HOME = "/home/admin";

    // teamCode
    String PROD_TEAM_CODE = "78706iroxp";

    //// 赛题数据：1.txt到10.txt
    String PROD_DATA_HOME = PROD_HOME + "/canal_data/";

    //日志目录
    String PROD_LOG_HOME = PROD_HOME + "/logs/" + PROD_TEAM_CODE + "/";

    // 中间结果目录（client和server都会用到）
    String PROD_MIDDLE_HOME = PROD_HOME + "/middle/" + PROD_TEAM_CODE + "/";

    // 结果文件目录(client端会用到)
    String PROD_RESULT_HOME = PROD_HOME + "/sync_results/" + PROD_TEAM_CODE + "/";

    //结果文件的命名
    String PROD_RESULT_FILE_NAME = "Result.rs";

}
